import re
import json
import csv
from collections import defaultdict

log_file = 'hardware_log.log'
output_csv = 'parsed_log.csv'
statistics_file = 'log_statistics.csv'

log_pattern = re.compile(
    r'(?P<timestamp>\w{3} \d{2} \d{2}:\d{2}:\d{2}) '
    r'(?P<hostname>\w+) '
    r'(?P<process_name>\w+)\[(?P<process_id>\d+)\]: '
    r'\[(?P<log_timestamp>\d+\.\d+)\] '
    r'(?P<syslog_error_level>[<>0-9]+) '
    r'(?P<log_contents>.*)'
)

def parse_log_line(line):
    match = log_pattern.match(line)
    if match:
        log_data = match.groupdict()
        if log_data['log_contents'].startswith('{'):
            try:
                log_data['log_contents'] = json.loads(log_data['log_contents'])
            except json.JSONDecodeError as e:
                print(f"JSON decode error: {e} in line: {line}")
        return log_data
    else:
        print(f"Line didn't match pattern: {line}")
        return None

parsed_logs = []
event_count = defaultdict(int)

with open(log_file, 'r') as file:
    for line in file:
        parsed_line = parse_log_line(line)
        if parsed_line:
            parsed_logs.append(parsed_line)
            if isinstance(parsed_line['log_contents'], dict) and 'event' in parsed_line['log_contents']:
                event_count[parsed_line['log_contents']['event']] += 1

with open(output_csv, 'w', newline='') as csvfile:
    fieldnames = ['timestamp', 'hostname', 'process_name', 'process_id', 'log_timestamp', 'syslog_error_level', 'log_contents']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for log in parsed_logs:
        writer.writerow(log)

with open(statistics_file, 'w', newline='') as csvfile:
    fieldnames = ['event', 'count']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for event, count in event_count.items():
        writer.writerow({'event': event, 'count': count})

print("Log parsing completed. Parsed data saved to parsed_log.csv and statistics saved to log_statistics.csv.")
