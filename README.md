# Contents:

**event_count_statistics.png** - visualized output with some basic stats about the number of certain events from the logs

**event_count_statistics_terminal.png** - same, but from the terminal view

**hardware_log.log** - main log file

**log_parser.py** - main parser file to create .csv files

**log_statistics.csv**	- stats about the number of events from hardware_log

**parsed_log.csv** - all the logs from hardware_log.log gathered in a table in csv format

**visualize_log.py** - visualize data from log_statistics.csv into event_count_statistics.png

## Final output in png 

![image info](/event_count_statistics.png)

## Screenshot from terminal for additional readability

![image info](/event_count_statistics_terminal.png)