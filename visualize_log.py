import pandas as pd
import matplotlib.pyplot as plt

# Load the parsed log data
parsed_log_df = pd.read_csv('parsed_log.csv')
statistics_df = pd.read_csv('log_statistics.csv')

# Display the parsed log data
print("Parsed Log Data:")
print(parsed_log_df.head())

# Display the event count statistics
print("Event Count Statistics:")
print(statistics_df)

# Plot the event counts
plt.figure(figsize=(14, 8))  # Increase the figure size
plt.bar(statistics_df['event'], statistics_df['count'])
plt.xlabel('Event')
plt.ylabel('Count')
plt.title('Event Count Statistics')
plt.xticks(rotation=45, ha='right')  # Rotate x-ticks for better visibility
plt.tight_layout()  # Adjust the layout to make room for the rotated labels
plt.savefig('event_count_statistics.png')  # Save the figure as a PNG file
plt.show()

